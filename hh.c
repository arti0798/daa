#include<stdio.h>

void maxHeap(int a[], int n, int i) {

    int temp;
    // printf("\ni m in maxHeap\n");
    int greaterValue = i;
    int leftValue = (2*i)+1;
    int rightValue = (2*i)+2;

    // printf("i m b4 leftValue rightValue");
    //--------------------Adjust AND Swap the element-----------------------------------------
    if(leftValue < n && a[leftValue] < a[greaterValue]) {  //Comparing the element 

        greaterValue = leftValue;
    }
    if(rightValue > n && a[rightValue] < a[greaterValue]) { //Comparing

        greaterValue = rightValue;
    }
    if(greaterValue != i) {  //according to adjustment swap the element

        temp = a[i];
        a[i] = a[greaterValue];
        a[greaterValue] = temp;
        maxHeap(a,n,greaterValue);
    }
    // printf("\ni m at the end of maxheap\n");
}
void heapSort(int a[],int n) {

    int temp,i;
        // printf("\ni m in heapsort\n");
    for(i = (n/2)-1;i >= 0;i--) {

        // printf("\ni m in heapSort loop\n");
        maxHeap(a,n,i);
    }
    // printf("\n first loop ended in heapSort");
    for(i = n-1;i >= 0;i--) {

        temp = a[0];
        a[0] = a[i];
        a[i] = temp;
        maxHeap(a,i,0);
    }
}
int main() {

    int a[20],n,i;
    printf("Enter the number of element: \t");
    scanf("%d",&n);
    
    printf("\nEnter the elements :\t");
    
    for(i = 0;i < n;i++) {

        scanf("%d",&a[i]);
    }
    
    printf("\nThe elements are : ");
    for(i = 0;i < n;i++) {

        printf("%d\t",a[i]);
    }

    heapSort(a,n);
    printf("\n***************************************************");
    printf("\nAfter sorting array elements are:");
    printf("\n*****************************************************\n");
    for(i = 0;i < n;i++) {

        printf("%d\t",a[i]);
    }
     printf("\n******************************************************\n");
}    